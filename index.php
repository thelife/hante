<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
    <title>授课学堂，企业学堂，企业云学堂-授课网</title>
    <meta name="keywords" content="授课学堂，授课学堂，企业云学堂-授课网">
    <meta name="description" content="授课网是一家最专业为企业提供在线培训平台的提供商。授课学堂提供了从学员管理、培训课程安排、培训效果考核与学员在线考试等人力资源事务的全流程管理。搜客顺应企业培训碎片化、移动化、游戏化和社区化的需求、为企业提供学习地图、享学社区、电脑手机同步学习等功能。">
	<link rel="stylesheet" href="css/main.css">
</head>
<body>
	<?php
        include './header.html';
    ?>
	<!-- 幻灯片 -->
	<div class="flexslider">
		<ul class="slides">
            <li style="position:relative;background:#001739 url(images/banner3.jpg) 50% 0 no-repeat;"><a id="ding" target="_blank" href=" https://www.dingtalk.com?lwfrom=20171026164624180"></a></li>
			<li style="background:#0077C9 url(images/banner1.jpg) 50% 0 no-repeat;">
				<div class="w940 por">
					<a href="./product.php" class="poa btn1">查看详情</a>
				</div>
			</li>
			<li style="background:#F2F2F2 url(images/banner2.jpg) 50% 0 no-repeat;">
				<div class="w940 por">
					<a target="_blank" href="http://mudu.tv/watch/1012048" class="poa btn2">查看详情</a>
				</div>
			</li>
		</ul>
	</div>
	<!-- 我们提供什么 -->
	<div class="m-abilities">
        <h2 class="title">我们提供什么?</h2>
        <p class="cap">培训行业最贴近企业端学习需求的LMS平台</p>
        <ul class="abilities clearfix">
            <li class="link">
                <a class="anchor">
                    <div class="iconw">
                        <span><img src="./images/icon/1.png" alt="SAAS学习平台"></span>
                        <i class="u-icon u-iconh u-icon-imh"></i>
                    </div>
                    <p class="name">SAAS学习平台</p>
                    <p class="detail">30分钟搭建企业大学 ，自动升级，无<br>需技术维护，24小时即时服务</p>
                </a>
            </li>
            <li class="link">
                <a class="anchor">
                    <div class="iconw">
                        <img src="./images/icon/2.png" alt="企业私有学习平台">
                    </div>
                    <p class="name">企业私有学习平台</p>
                    <p class="detail">私有云部署，定制服务，市场上唯一不限<br>用户数量的LMS</p>
                </a>
            </li>
            <li class="link">
                <a class="anchor">
                    <div class="iconw">
                        <span><img src="./images/icon/3.png" alt="专题式学习"></span>
                    </div>
                    <p class="name">专题式学习</p>
                    <p class="detail">支持新员工智能学习匹配、肢体聚合<br>学习、学习考试结合等多种<br>专题式学习</p>
                </a>
            </li>
        </ul>
        <ul class="abilities mt50 clearfix">
            <li class="link">
                <a class="anchor">
                    <div class="iconw">
                        <span><img src="./images/icon/4.png" alt="最全线上线下学习活动"></span>
                    </div>
                    <p class="name">最全线上线下学习活动</p>
                    <p class="detail">e-learning、微课、考试、直播、面授<br>学习、专题学习、党建学习、证书、线下<br>学分导入等多种学习活动结合</p>
                </a>
            </li>
            <li class="link">
                <a href="zhishiguanli.html" class="anchor">
                    <div class="iconw">
                        <span><img src="./images/icon/5.png" alt="企业知识管理"></span>
                    </div>
                    <p class="name">移动端学习</p>
                    <p class="detail">随时随地利用碎片化时间实现掌上学<br>习，解决工训矛盾</p>
                </a>
            </li>
            <li class="link">
                <a href="shehuihuaxuexi.html" class="anchor">
                    <div class="iconw">
                        <span><img src="./images/icon/6.png" alt="企业社会化学习"></span>
                    </div>
                    <p class="name">党建学习</p>
                    <p class="detail">专门公检法事业单位开启党建学习，<br>集合学习、考试、心得，更好监测<br>学习效果</p>
                </a>
            </li>
        </ul>
    </div>
    <!-- 面向中小型企业 -->
	<div class="m-sure">
        <h2 class="title">面向中小型企业，无缝对接钉钉，辅助企业学习管理，<br>助力企业打造学习型创新组织，适应不断变化的人才和市场竞争</h2>
        <p class="cap">已稳定支持1万家企事业单位，600万用户在线学习</p>
        <ul class="sures clearfix">
            <li class="sure">
                <div class="iconw u-vmw">
                    <i class="u-icon u-icon-thread u-vm"></i>
                </div>
                <p class="name">新员工</p>
                <p class="detail">
                    为新员工批量设置标识字段，智能匹配<br>
                    新员工学习课程
                </p>
            </li>
            <li class="sure">
                <div class="iconw u-vmw">
                    <i class="u-icon u-icon-global u-vm"></i>
                </div>
                <p class="name">关键岗位</p>
                <p class="detail">
                    为关键岗位指派匹配学习内容，以考促学，<br>
                    跟踪学习进度，企业培训管理好工具
                </p>
            </li>
            <li class="sure">
                <div class="iconw u-vmw">
                    <i class="u-icon u-icon-sure u-vm"></i>
                </div>
                <p class="name">PC端、移动端学习</p>
                <p class="detail">
                    手机实时提醒，碎片化时间充分利用，<br>
                    PC端和移动端学习相结合，每天提升<br>
                    进步
                </p>
            </li>
        </ul>
    </div>
	<!-- 人工服务 -->
	<div class="m-intime">
        <div class="bg">
            <div class="img" style="background-position: center -24px;"></div>
            <div class="mask"></div>
        </div>
        <div class="cnt">
            <h2 class="title blueBg">人工服务，即时响应</h2>
            <p class="cap">专家级技术团队提供专业服务</p>
            <ul class="intimes">
                <li class="intime">
                    <p>
                        <i class="u-icon u-icon-intime u-icon-intime1"></i>
                    </p>
                    <p>1对1技术支持</p>
                </li>
                <li class="intime">
                    <p>
                        <i class="u-icon u-icon-intime u-icon-intime2"></i>
                    </p>
                    <p>1个小组交付实施</p>
                </li>
                <li class="intime">
                    <p>
                        <i class="u-icon u-icon-intime u-icon-intime3"></i>
                    </p>
                    <p>7x24小时运维</p>
                </li>
                <li class="intime">
                    <p>
                        <i class="u-icon u-icon-intime u-icon-intime4"></i>
                    </p>
                    <p>全天候在线咨询</p>
                </li>
            </ul>
        </div>
    </div>
    <!-- 节省您的时间 -->
	<div class="save por">
		<div class="s-tit">节省您的时间，提高您处理培训事务的效率</div>
		<div class="poa s-t1"><span>●</span>使用“授课学堂”，可显著降低面授课      程的教学准备时间，可减少2/3的学习项目管理时间。 </div>
		<div class="poa s-t2"><span>●</span>让您1人=3人从此可以关注更有价值的培训专业事务。</div>
	</div>
    <?php
        include './footer.html';
    ?>
	<!-- 右侧浮动导航 -->
	<div class="right-nav">
		<ul>
			<li  class="por ex-wrap"><a href="##" class="rn-1">在线咨询</a><img src="images/ecode3.png" class="experience ex-show"></li>
			<li class="por ex-wrap"><a href="##" class="rn-2">客户热线</a><div class="ex-tel ex-show">0532-88983839</div></li>
			<li class="por ex-wrap"><a href="##" class="rn-3">立即体验</a><img src="images/ecode2.png" class="experience ex-show"></li>
			<li><a href="##" class="rn-4" id="gotop">返回顶部</a></li>
		</ul>
	</div>

	<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
	<script type="text/javascript" src="js/jquery.flexslider-min.js"></script>
	<script type="text/javascript" src="js/common.js"></script>
	<script type="text/javascript">
		$(function(){
			$('.flexslider').flexslider({
				directionNav: true,
				pauseOnAction: false,
				animation: "fade",
				slideshowSpeed: 5000,
				before: function(e) {
					console.log(e);
					$(".flex-control-nav li").eq(e.animatingTo).width(40).siblings().width(10);
				},
			});
		});
	</script>
</body>
</html>
 <script type="text/javascript" src="js/tongji.js"></script>

