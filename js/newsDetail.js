$(function(){
    var laytpl = layui.laytpl;
    var protocol = window.location.protocol;
    var host     = window.location.host;
    // var request  = protocol+"//"+host+'/Api/v1.0/index.php';

    // 测试代码
    // var request  = 'http://saas.eduto.com/Api/v1.0/index.php/';

     // 正式代码
    var request = 'http://saas.soke.cn/Api/v1.0/index.php';

    // 获取参数
    getParam = function(name){
        var search = document.location.search;
        var pattern = new RegExp("[?&]"+name+"\=([^&]+)", "g");
        var matcher = pattern.exec(search);
        var items = null;
        if(null != matcher){
            try{
                items = decodeURIComponent(decodeURIComponent(matcher[1]));
            }catch(e){
                try{
                    items = decodeURIComponent(matcher[1]);
                }catch(e){
                    items = matcher[1];
                }
            }
        }
        return items;
    };
    var uuid = getParam('uuid');

    // 获取数据的方法
    function getData(){
        $.ajax({
            type:'GET',
            url:request+'/User/Article/articleInfo',
            dataType:'jsonp',
            jsonp:'callback',  
            jsonpCallback:'jsonpCall',
            data:{
                uuid:uuid
            },
            success:function(res){
                
                // 根据类型选中对应的tab
                var type = res.data.info.type;
                $(".news-tab li[data-type="+type+"]").addClass('active').siblings().removeClass('active');

                // 数据渲染
                var cont = res.data.info;
                // 设置网页title
                $("title").html(res.data.info.title);
                var getTpl = document.getElementById('content').innerHTML,view = document.getElementById('news-cont');
                laytpl(getTpl).render(cont, function(html){
                  view.innerHTML = html;
                });
            }
        })
    }
    getData();


})