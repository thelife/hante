;!function(){
	var laytpl = layui.laytpl;
	var layload = layui.flow;
	var protocol = window.location.protocol;
    var host     = window.location.host;
    // var request  = protocol+"//"+host+'/Api/v1.0/index.php';
    var tab1LoadEnd = false;
    var tab2LoadEnd = false;
    var tab3LoadEnd = false;
    var itemIndex = 0;

    // 测试代码
    // var request  = 'http://saas.eduto.com/Api/v1.0/index.php/';
    
    // 正式代码
   	var request = 'http://saas.soke.cn/Api/v1.0/index.php';

	// 获取参数
    getParam = function(name){
        var search = document.location.search;
        var pattern = new RegExp("[?&]"+name+"\=([^&]+)", "g");
        var matcher = pattern.exec(search);
        var items = null;
        if(null != matcher){
            try{
                items = decodeURIComponent(decodeURIComponent(matcher[1]));
            }catch(e){
                try{
                    items = decodeURIComponent(matcher[1]);
                }catch(e){
                    items = matcher[1];
                }
            }
        }
        return items;
    };
    var urlType = getParam('type');

	var infoType = 'example';
    if(urlType){
    	infoType = urlType;
   	 	$(".news-tab li[data-type="+infoType+"]").addClass('active').siblings().removeClass('active');
    	// 获取新数据
    	nowPage=1;
    	getData();
    }
    // 分类标识(news-新闻，example-客户案例，version-升级公告)
    

    // 获取数据的方法
    var nowPage = 1;
    function getData(){
    	// 清空原分类数据
    	$("#news-list").empty();
    	$('.dropload-down').remove();
    	dropload = $('.news-list-wrap').dropload({
		    scrollArea : window,
		    domDown	   : {
		    	domClass : 'dropload-down',
				domRefresh : '<div class="dropload-refresh">下拉加载更多</div>',
				domLoad : '<div class="dropload-load">加载中...</div>',
				domNoData : '<div class="dropload-noData">加载完毕</div>'
		    },
			threshold:400,	
		    loadDownFn : function(me){
		        $.ajax({
		            type:'GET',
			    	url:request+'/User/Article/index',
			    	dataType:'jsonp',  
			    	jsonp:'callback',  
			    	jsonpCallback:'jsonpCall',
			    	data:{
			    		type:infoType,
			    		page:nowPage
			    	},
		            success: function(res){
			    		var list = res.data.list;
			    		if(list.length > 0){
			    			var getTpl = document.getElementById('list').innerHTML,view = document.getElementById('news-list');
							laytpl(getTpl).render(list, function(html){
							  view.innerHTML = html;
							});
			                nowPage++;
							// 每次数据加载完，必须重置
			                me.resetload();
			    		}else{
			    			/*if(itemIndex == 0){
			    				tab1LoadEnd = true;
			    			}else if(itemIndex == 1){
			    				tab2LoadEnd = true;
			    			}else if(itemIndex == 2){
			    				tab3LoadEnd = true;
			    			}*/
			    			if(nowPage==1){
			    				// 锁定
		                        me.lock();
		                        // 无数据
		                        me.noData();
		                        me.resetload();
			    				$(".dropload-noData").html("暂无数据");
			    			}else{
			    				// 锁定
		                        me.lock();
		                        // 无数据
		                        me.noData();
		                        me.resetload();
			    			}
			    			
			    		}
		            },
		            error: function(e){
		                console.log(e);
		                // 即使加载出错，也得重置
		                me.resetload();
		            }
		        });
		    }
		});
    }
    getData();

    // 切换分类时，重新请求数据
    $(".news-tab li").on('click',function(){
    	infoType = $(this).data('type');

    	$(this).addClass('active').siblings().removeClass('active');

    	// 获取新数据
    	nowPage=1;
    	getData();
    })
}();