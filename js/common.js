// 根据地址判断当前页面，标记选中状态
var url = window.location.pathname;

switch (true){
	// 首页
	case url.indexOf('index') >= 0 : 
		$(".nav li:eq(0) a").addClass("nav-active");
		$(".nav li:eq(0)").siblings().children("a").removeClass();
		break;
	//产品
	case url.indexOf('product') >= 0 : 
		$(".nav li:eq(1) a").addClass("nav-active");
		$(".nav li:eq(1)").siblings().children("a").removeClass();
		break;
	//下载
	case url.indexOf('download') >= 0 : 
		$(".nav li:eq(2) a").addClass("nav-active");
		$(".nav li:eq(2)").siblings().children("a").removeClass();
		break;
	
	// 企业动态
	case url.indexOf('news') >= 0 : 
		
		$(".nav li:eq(3) a").addClass("nav-active");
		$(".nav li:eq(3)").siblings().children("a").removeClass();
		break;
	// 登录
	case url.indexOf('login') >= 0 : 
		$(".nav li:eq(4) a").addClass("nav-active");
		$(".nav li:eq(4)").siblings().children("a").removeClass();
		break;
	// 企业注册
	case url.indexOf('register') >= 0 : 
		$(".nav li:eq(5) a").addClass("nav-active");
		$(".nav li:eq(5)").siblings().children("a").removeClass();
		break;

	// 首页
	default : 
		$(".nav li:eq(0) a").addClass("nav-active");
		$(".nav li:eq(0)").siblings().children("a").removeClass();
		break;
}

$(function(){
	// 返回顶部
	$("#gotop").on('click',function(){
		$("html,body").animate({
			scrollTop:0
		},300)
	});
	// 立即体验滑过
	$(".ex-wrap").hover(function(){
		$(this).find(".ex-show").show();
	},function(){
		$(this).find(".ex-show").hide();
	})

})