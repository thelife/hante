<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title></title>
	<link rel="stylesheet" href="css/main.css">
	<style type="text/css">
		body{
			background-color: #F9F9F9;
		}
	</style>
</head>
<body>
	<?php
		include 'header.html';
	?>
	<div class="news-ban">
		<div class="news-tab-wrap">
			<ul class="news-tab">
				<li data-type="example"><a href="./news.php?type=example">客户案例</a></li>
				<li data-type="news"><a href="./news.php?type=news">新闻资讯</a></li>
				<li data-type="version"><a href="./news.php?type=version">升级公告</a></li>
			</ul>
		</div>
	</div>
 	<div class="news-cont" id="news-cont">
 		<!-- <div class="cont-logo">
 			      <img class="avatar_logo" src="https://dn-ikcrm-files.qbox.me/crm_cases/logos/1/u_36878105_3754655826_fm_214_gp_0.jpg" alt="U 36878105 3754655826 fm 214 gp 0">
 			    </div>
 			    <div class="cont-right">
 			    	<h3 class="cont-tit">降低时间成本，提高员工效率管理</h3>
 			    	<p>测试文字测试文字测试文字测试文字测试文字测试文字测试文字测试文字测试文字测试文字测试文字测试文字测试文字测试文字测试文字测试文字测试文字测试文字测试文字测试文字</p>
 			    	<img src="https://dn-ikcrm-files.qbox.me/ueditor_images/files/627/1440485439749.jpg" title="" alt="1440485439749.jpg">
 			    </div> -->
 	</div>

	<?php
		include 'footer.html';
	?>
	<!-- 内容模板 -->
	<script id="content" type="text/html">
		
	 	<div class="cont-logo">
	      <img class="avatar_logo" src="{{d.imgPath}}" >
	    </div>
	    <div class="cont-right">
	    	<h3 class="cont-tit">{{d.title}}</h3>
	    	{{d.content}}
	    </div>
		
	</script>
	<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
	<script type="text/javascript" src="js/common.js"></script>
	<script src="js/layui/layui.all.js"></script>
	<script src="js/newsDetail.js"></script>
</body>
</html>