<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>企业动态</title>
	<link rel="stylesheet" href="js/dropload/dropload.css">
	<link rel="stylesheet" href="css/main.css">
</head>
<body>
	<?php
		include 'header.html';
	?>
	<div class="news-ban">
		<div class="news-tab-wrap">
			<ul class="news-tab">
				<li class="active" data-type="example">客户案例</li>
				<li data-type="news">新闻资讯</li>
				<li data-type="version">升级公告</li>
			</ul>
		</div>
	</div>
 	<div class="news-list-wrap">
 		<ul class="news-list news-center" id="news-list">
 			<!-- <li>
 				<img src="images/computy.png">
 				<div class="news-right">
 					<div class="news-r-tit">测试标题</div>
 					<div class="news-r-info">
 						摘要摘要摘要摘要摘要摘要摘要摘要摘要摘要摘要摘要摘要摘要摘要摘要摘要摘
 					</div>
 					<a class="news-more">查看更多 ></a>
 				</div>
 			</li> -->
 		</ul>
 	</div>

	<?php
		include 'footer.html';
	?>
	
	<!-- 列表模板 -->
	<script id="list" type="text/html">
		{{# layui.each(d, function(index, item){ }}
	 	<li>
			<img src="{{ item.imgPath }}">
			<div class="news-right">
				<div class="news-r-tit"><a href="./newsDetail.php?uuid={{item.uuid}}">{{ item.title }}</a></div>
				<div class="news-r-info">
					{{item.introduction}}
				</div>
				<a class="news-more" href="./newsDetail.php?uuid={{item.uuid}}">查看更多 ></a>
			</div>
		</li>
		{{# }) }}
	</script>
	<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
	<script type="text/javascript" src="js/common.js"></script>
	<script src="js/layui/layui.all.js"></script>

	<script src="js/dropload/dropload.min.js"></script>
	<script src="js/news.js"></script>
</body>
</html>