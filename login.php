<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>登录</title>
	<link rel="stylesheet" href="css/main.css">
	 <script src="http://g.alicdn.com/dingding/dinglogin/0.0.2/ddLogin.js"></script>
	 <style type="text/css">
      
        .es-header{
            background: #28c195;
            height: 60px;
            border: none;
            padding: 0 20px;
            line-height: 60px;
            overflow:hidden;
        }
        .es-header img{
            height: 50px;
            width: auto;
            margin-top: 5px;
            margin-left: 10px;

        }
        .es-header a{
            display: inline-block;
            height: 100%;
            line-height: 60px;
            float: left;
            text-decoration: none;
            color: #fff;
            font-size: 18px;
        }
        .es-header a:nth-of-type(2){
            margin-left: 40px
        }
        .login .login-container {
            width: 446px;
            margin: 0 auto;
            position: relative;
            margin-top: 80px;
        }
        .heading {
            padding: 12px 55px 0px;
            color: #424242;
            margin-bottom: 35px;
        }
        .content{
            width: 500px;
            height: 500px;
            margin: 75px auto;
            background:#fff;
            position: relative;
            box-shadow: 5px 5px 5px #ced2d5;
            border: 1px solid #ccc;
        }
        .pc{
            width: 75px;
            height: 75px;
            position: absolute;
            right: 0;
            top: 0;
            background: url(__STATIC__/Images/Common/pc.png) no-repeat;
            background-size: cover;
        }
        .pc a{
            display: block;
            width: 100%;
            height: 100%;
        }
        .content h3{
            font-size: 16px;
            padding-left: 60px;
            height: 45px;
            line-height: 45px;
            margin-top: 20px;
            color: #333;
            overflow: hidden;
        }
        .content p{
            height: 20px;
            line-height: 20px;
            text-align: right;
            padding-right: 65px;
            color: #666;
            font-size: 14px;
            margin-top: 15px;
        }
        .content p a{
            color: #38adff;
            text-decoration: none;
        }
        iframe{
            height: 300px;
            width: 320px;
        }
        @media (max-width: 767px) {
            .es-wrap {
                width: 100%;
                height: 100%;
                overflow: hidden;
            }
            .content {
                width: 100% !important;
                height: 100% !important;
                background: #fff !important;
                position: relative;
                margin: 0 auto;
                box-shadow: 5px 5px 5px #ced2d5;
                border: 1px solid #ccc;
                border: none !important;
                box-shadow: none !important;
            }
            a#logActive {
                padding-left:15px !important;
                text-align: left;
            }
        }
        .open-now{
            font-size: 14px;
            color:#000;
        }
        .open-now a{
            font-size: 14px;
            color:#38ADFF;
        }
    </style>
</head>
<body>
	<?php
        include("header.html");
    ?>
     
     <div id="loginContent">
        <div class="login">
            <h1>开通授课学堂-钉钉版</h1>
            <div class="loginBox1 loginBox">
                <div class="center" style="overflow:hidden;">
                    <div id="login_container" style="margin:0 auto;width:320px;height: 320px;margin-top: 20px"></div>
                    <p class="open-now">还未开通钉钉版？点此<a href="https://h5.dingtalk.com/orgreg/isvIndex.html?qrCode=E7DB6709D3328D8D6A4263CE2D730843E5137E585AFC0E7F844FA8EA64D96280BAE29FE31BAF4BBC6EB49BF3A6658AFC3842286A5958D662B7C5296ADAF855CF7AF1FD0530BEB3F776FDA4EB6E3C5A0B96BB1DBF549ED26544D04C8E8370AB1D069B1E301FAEC198&signature=3816f35377ad36f2a3b8b45327392e5f58c228bf">立即开通</a></p>
                </div>
            </div>
        </div>
    </div>
        <?php
            include("footer.html");
        ?>
	
	<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
	<script type="text/javascript" src="js/jquery.flexslider-min.js"></script>
    <script type="text/javascript" src="js/common.js"></script>
	<script type="text/javascript">
		$('.loginBtn').click(function(){
			var loginBox = $(this).parents('.loginBox');
			loginBox.hide().siblings('.loginBox').show();
		})
	</script>
	<script type="text/javascript">
    var obj = DDLogin({
         id:"login_container",//这里需要你在自己的页面定义一个HTML标签并设置id，例如<div id="login_container"></div>或<span id="login_container"></span>
         goto: "https://oapi.dingtalk.com/connect/oauth2/sns_authorize?appid=dingoa1lvhzllidyfqqjsq&response_type=code&scope=snsapi_login&state=STATE&redirect_uri=http://www.soke.cn/User/index.php/Home/Login/dingDingLogin",
         style: "",
         href: "",
         width : "300px",
         height: "300px"
     });
    var hanndleMessage = function (event) {
        var loginTmpCode = event.data; //拿到loginTmpCode后就可以在这里构造跳转链接进行跳转了
        var origin = event.origin;
        var gotoUrl = "https://oapi.dingtalk.com/connect/oauth2/sns_authorize?appid=dingoa1lvhzllidyfqqjsq&response_type=code&scope=snsapi_login&state=STATE&redirect_uri=http://www.soke.cn/User/index.php/Home/Login/dingDingLogin";
        window.location.href=gotoUrl+"&loginTmpCode="+loginTmpCode;
    };
    
    if (typeof window.addEventListener != 'undefined') {
        window.addEventListener('message', hanndleMessage, false);
    } else if (typeof window.attachEvent != 'undefined') {
        window.attachEvent('onmessage', hanndleMessage);
    }
</script>
</body>
</html>