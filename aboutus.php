<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>首页</title>
    <link rel="stylesheet" type="text/css" href="iconfont/iconfont.css"/>
	<link rel="stylesheet" href="css/main.css">
</head>
<body>
	<?php
        include("header.html");
    ?>
    <!-- 内容区域 -->
    <div class= "serve1">
            <div class = "wrap" style="width:940px;">
                <div class = "title">关于我们</div>
            </div>
        </div>
        
        <div class = "serve2">
            <!-- <div class = "title_list">
                <p class = "active">关于我们</p><p>联系我们</p>
            </div> -->
            <div>
            <!-- <div class = "contact_us active_list">
                <div class = "wrap">
                    <ul>
                        <li>电话：4008-336-070</li>
                        <li>座机：0532-89602665</li>
                        <li>邮箱：sales@hantekeji.com</li>
                        <li>全国区域服务机构</li>
                    </ul>
                    <div id = "serve">
                        <div class = "serve_l">
                            <a class = "iconfont beijing">&#xe600;<br/><i>北京</i></a>
                            <a class = "iconfont qingdao">&#xe600;<br/><i>青岛</i></a>
                            <a class = "iconfont wuhan">&#xe600;<br/><i>武汉</i></a>
                            <a class = "iconfont chengdu">&#xe600;<br/><i>成都</i></a>
                            <a class = "iconfont ganzhou">&#xe600;<br/><i>赣州</i></a>
                            <a class = "iconfont guangzhou">&#xe600;<br/><i>广州</i></a>
                            <a class = "iconfont shenzhen">&#xe600;<br/><i>深圳</i></a>
                            <a class = "iconfont xiamen">&#xe600;<br/><i>厦门</i></a>
                        </div>
                        <div class = "serve_r">
                            <p>华东大区购买咨询</p>
                            <p>电话：4008-366-070</p>
                            <span>地址1：山东省青岛市开发区衡山路14号鸿润金汇泉商区中心18楼（总部）</span>
                            <span>地址2：山东省青岛市市北区和兴路59号太懂万佳广场2楼3-339</span>
                        </div>
                    </div>
                </div>
            </div> -->
            <div class = "about_us active_list">
                    <div class = "about1">
                        <div class = "wrap">
                            <div class = "title">企业介绍</div>
                            <div class = "introduce">
                                　　青岛翰特网络科技有限公司起源于广州，发展于青岛。自成立伊始，公司不断关注行业最新技术动态，重视产品技术升级和持续创新。<br/>
                            　　公司2013年申请取得企业学习系统相关８项产品著作权，并申请授课商标。2015年翰特科技获得中国（宁波）人力资源服务创新创业大赛三等奖，
                            2015年入选青岛千帆计划科技型企业，2016年获得亚太人力资源服务创新奖，2016年获得商标42类、35类、９类三个大类的“授课”商标授权，2016
                            年成为国家高新企业。<br/>
                            　　授课社群学习系统被劲酒集团、芙蓉王卷烟厂、方恩医药、合金资本、奥润顺达、差旅天下、养生堂等众多企业采购使用，为300万企业员工提供服务。
                            </div>
                        </div>
                    </div>
                    <div class = "about2">
                        <div class = "wrap">
                            <div class = "title">荣誉资质</div>
                            <div class = "honor">
                                <dl>
                                    <dt><img src="images/honor3_03.png" alt="国家高新企业认证证书" /></dt>
                                    <dd>国家高新企业认证证书</dd>
                                </dl>
                                <dl>
                                    <dt><img src="images/honor2.png" alt="CMMI3国际管理体系证书" /></dt>
                                    <dd>CMMI3国际管理体系证书</dd>
                                </dl>
                                <dl>
                                    <dt><img src="images/honor3_05.png" alt="2016年亚太人力资源创新服务大赛创新奖" /></dt>
                                    <dd>2016年亚太人力资源创新服务大赛创新奖</dd>
                                </dl>
                                <dl>
                                    <dt><img src="images/honor3_07.png" alt="2015年亚太人力资源创新服务大赛三等奖" /></dt>
                                    <dd>2015年亚太人力资源创新服务大赛三等奖</dd>
                                </dl>
                                <dl>
                                    <dt><img src="images/honor1.png" alt="无忧商务软件" /></dt>
                                    <dd>无忧商务软件</dd>
                                </dl>
                                <dl>
                                    <dt><img src="images/honor1.png" alt="悬系管理系统软件" /></dt>
                                    <dd>悬系管理系统软件</dd>
                                </dl>
                                <dl>
                                    <dt><img src="images/honor1.png" alt="无忧微商盟系统软件" /></dt>
                                    <dd>无忧微商盟系统软件</dd>
                                </dl>
                                <dl>
                                    <dt><img src="images/honor1.png" alt="企业网院ForAndriod系统软件" /></dt>
                                    <dd>企业网院ForAndriod系统软件</dd>
                                </dl>
                                <dl>
                                    <dt><img src="images/honor1.png" alt="无忧FLASH阅读器系统软件" /></dt>
                                    <dd>无忧FLASH阅读器系统软件</dd>
                                </dl>
                                <dl>
                                    <dt><img src="images/honor3_13.png" alt="企业学习系统软件" /></dt>
                                    <dd>企业学习系统软件</dd>
                                </dl>
                                <dl>
                                    <dt><img src="images/honor3_13.png" alt="授课独立招生系统软件" /></dt>
                                    <dd>授课独立招生系统软件</dd>
                                </dl>
                                <dl>
                                    <dt><img src="images/honor3_13.png" alt="授课课件制作工具软件" /></dt>
                                    <dd>授课课件制作工具软件</dd>
                                </dl>
                                    <dl>
                                    <dt><img src="images/honor1.png" alt="企业网院ForAndriod系统软件" /></dt>
                                    <dd>企业网院ForAndriod系统软件</dd>
                                </dl>
                                <dl>
                                    <dt><img src="images/honor1.png" alt="授课学习活动管理软件" /></dt>
                                    <dd>授课学习活动管理软件</dd>
                                </dl>
                                <dl>
                                    <dt><img src="images/honor3_18.jpg" alt="黑马大赛晋级奖" /></dt>
                                    <dd>黑马大赛晋级奖</dd>
                                </dl>
                                <dl>
                                    <dt><img src="images/honor3_17.png" alt="授课商标注册" /></dt>
                                    <dd>授课商标注册</dd>
                                </dl>
                            </div>
                        </div>
                    </div>                  
            </div>
            </div>
        </div>



	<?php
        include("footer.html");
    ?>
	
	<!-- 右侧浮动导航 -->
	<div class="right-nav">
		<ul>
			<li  class="por ex-wrap"><a href="##" class="rn-1">在线咨询</a><img src="images/ecode3.png" class="experience ex-show"></li>
			<li class="por ex-wrap"><a href="##" class="rn-2">客户热线</a><div class="ex-tel ex-show">0532-88983839</div></li>
			<li class="por ex-wrap"><a href="##" class="rn-3">立即体验</a><img src="images/ecode2.png" class="experience ex-show"></li>
			<li><a href="##" class="rn-4" id="gotop">返回顶部</a></li>
		</ul>
	</div>

	<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
	<script type="text/javascript" src="js/jquery.flexslider-min.js"></script>
	<script type="text/javascript" src="js/common.js"></script>
    <script type="text/javascript">
        $(function(){
            /*$(".title_list").find("p").click(function(){
                $(this).attr('class','active').siblings().removeClass();
                var o = $(this).index();
                $(".active_list").eq(o).show().siblings().hide();
            })*/
        })
    </script>
</body>
</html>
 <script type="text/javascript" src="js/tongji.js"></script>

