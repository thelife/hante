<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>企业注册</title>
	<link rel="stylesheet" href="css/main.css">
</head>
<body>
	<?php
        include("header.html");
    ?>
	<div id='loginContent'>
		<div class='login'>
			<h1>开通授课学堂-钉钉版</h1>
			<div class='loginBox1 loginBox'>
				<p class='right loginBtn'>
					<img src="images/loginImg1.png">
					<span><i></i>钉钉老用户点这里</span>
				</p>
				<div class='center'>
					<button>新用户</button>
					<dl>
						<dt><img src="images/loginImg4.png"></dt>
						<dd>尚未注册钉钉的用户</dd>
						<dd>扫码注册或<a href='https://h5.dingtalk.com/orgreg/isvIndex.html?qrCode=E7DB6709D3328D8D6A4263CE2D730843E5137E585AFC0E7F844FA8EA64D96280BAE29FE31BAF4BBC6EB49BF3A6658AFC3842286A5958D662B7C5296ADAF855CF7AF1FD0530BEB3F776FDA4EB6E3C5A0B96BB1DBF549ED26544D04C8E8370AB1D069B1E301FAEC198&signature=3816f35377ad36f2a3b8b45327392e5f58c228bf'>点击注册</a></dd>
					</dl>
				</div>
			</div>
			<div class='loginBox2 loginBox'>
				<p class='right loginBtn'>
					<img src="images/loginImg1.png">
					<span><i></i>尚未注册钉钉点这里</span>
				</p>
				<div class='center'>
					<button>老用户</button>
					<dl>
						<dt><img src="images/loginImg4.png"></dt>
						<dd>钉钉扫描此二维码</dd>
						<dd>授权开通授课学堂</dd>
					</dl>
				</div>
			</div>
		</div>
	</div>
	<?php
        include("footer.html");
    ?>
	<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
	<script type="text/javascript" src="js/jquery.flexslider-min.js"></script>
	<script type="text/javascript" src="js/common.js"></script>
	<script type="text/javascript">
		$('.loginBtn').click(function(){
			var loginBox = $(this).parents('.loginBox');
			loginBox.hide().siblings('.loginBox').show();
		})
	</script>
</body>
</html>
 <script type="text/javascript" src="js/tongji.js"></script>

