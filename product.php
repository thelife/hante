<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>产品</title>
	<link rel="stylesheet" href="css/main.css">
</head>
<body>
	<?php
        include("header.html");
    ?>
    <div class="product-banner"></div>

    <div id="product-wrap">
		<div id = 'dingding' class='content'>
			<div class='wrap'>
				<div class='dingdingTop'>
					<h1>授课学堂+钉钉，一个全新的企业培训模式</h1>
					<h4>多端同步、多角色管理、轻松培训，以学促考，企业培训管理好工具</h4>
				</div>
				<div class='dingdingBottom'>
					<img src="images/img1.png">
					<dl>
						<dt>新员工学习，智能匹配</dt>
						<dd>●  为新员工批量设置标识字段</dd>
						<dd>●  智能匹配新员工学习课程</dd>
					</dl>
				</div>
			</div>
		</div>
		<div id='study' class='content'>
			<div class='wrap'>
				<dl>
					<dt>学习，多方式结合</dt>
					<dd>●  支持多类型课件格式，阿里云视频自动转码 </dd>
					<dd>●  优质的免费学习资源</dd>
					<dd>●  线上课程、专题学习、线下培训多种学习方式</dd>
					<dd>●  直播课</dd>
					<dd>●  学分统计、个人部门排行</dd>
					<dd>●  学习跟踪、数据统计</dd>
				</dl>
				<img src="images/img2.png">
			</div>
		</div>
		<div id='exam' class='content'>
			<div class='wrap'>
				<img src="images/img3.png">
				<dl>
					<dt>考试，方便强大</dt>
					<dd>●  支持单选、多选、判断、填空、简答、案例六大题型</dd>
					<dd>●  考试未达标可去重修关联课程 </dd>
					<dd>●  客观题、填空题自动阅卷 </dd>
					<dd>●  支持正式考试、练习考试</dd>
					<dd>●  一人一卷，随机抽题</dd> 
					<dd>●  考试防作弊机制</dd> 
				</dl>
			</div>
		</div>
		<div id='weike' class='content'>
			<div class='wrap'>
				<dl>
					<dt>微课，轻松做课</dt>
					<dd>●  学员可在移动端添加图片、语音、文字<br/>轻松制作微课</dd>
					<dd>●  促进企业知识文化沉淀，优化培训体系</dd>
				</dl>
				<img src="images/img5.png">
			</div>
		</div>
		<div id='Jurisdiction' class='content'>
			<div class='wrap'>
				<img src="images/img4.png">
				<dl>
					<dt>权限，自由设置</dt>
					<dd>●  可设置多角色功能权限，满足企业培训复杂的<br/>多样性，真正让培训系统“活”起来</dd>
				</dl>
			</div>
		</div>
		<div id='soke' class='content'>
			<div class='wrap'>
				<dl>
					<dt class='top'>启用授课学堂钉钉版</dt>
					<dd>钉钉扫码  立即启用授课学堂  开启企业培训新模式</dd>
					<div class='bottom'>客服在线：0532-88983839</div>
				</dl>
				<img src="images/img6.png">
			</div>
			
		</div>
	</div>
	
	<?php
        include("footer.html");
    ?>
	<!-- 右侧浮动导航 -->
	<div class="right-nav">
		<ul>
			<li  class="por ex-wrap"><a href="##" class="rn-1">在线咨询</a><img src="images/ecode3.png" class="experience ex-show"></li>
			<li class="por ex-wrap"><a href="##" class="rn-2">客户热线</a><div class="ex-tel ex-show">0532-88983839</div></li>
			<li class="por ex-wrap"><a href="##" class="rn-3">立即体验</a><img src="images/ecode2.png" class="experience ex-show"></li>
			<li><a href="##" class="rn-4" id="gotop">返回顶部</a></li>
		</ul>
	</div>

	<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
	<script type="text/javascript" src="js/common.js"></script>
</body>
</html>
 <script type="text/javascript" src="js/tongji.js"></script>

